﻿#include <iostream>
#include <fstream>
#include <time.h>
#include <string>
#include <sstream>
using namespace std;

struct datetime
{
    int hour, minute, second, day, month, year;
    void next_date(datetime*); 
    void prev_date(datetime*); 
    void print_date(datetime); 
    void print_info(datetime*, int);
    void print_info_prev(datetime*, int); 
};

void get_date(datetime*, int, int, int, int, int, int);
bool check_day(int, int, int); 
bool check_month(int, int); 
//bool check_year(int, int); 
int get_count_lines(string);
string** read_file(string, int);

int main()
{
    setlocale(LC_ALL, "Russian");
    string file = "data.txt";
    int count_lines = get_count_lines(file);
    string** lines = read_file(file, count_lines);
    datetime* dates = new datetime[count_lines];


    for (int i = 0; i < count_lines; i++)
    {
        get_date(&dates[i], stoi(lines[i][0]), stoi(lines[i][1]), stoi(lines[i][2]), stoi(lines[i][3]), stoi(lines[i][4]), stoi(lines[i][5]));
    } 

    for (int i = 0; i < count_lines; i++)
    {
            dates[i].print_info(&dates[i], i + 1);
        
    }

    cout << "\n\t\t\t\tВариант №7" << endl 
         << "\tДаты, для которых предыдущие не попадают на другой месяц.\n\n";
    for (int i = 0; i < count_lines; i++)
    {
        if (check_day(dates[i].month, dates[i].day, dates[i].year))
            dates[i].print_info_prev(&dates[i], i + 1);
    }



    for (int i = 0; i < count_lines; i++)
    {
        delete[] lines[i];
    }
    delete[] lines;

    return 0;
}

void get_date(datetime* date, int hour, int minute, int second, int day, int month, int year)
{
    if (hour < 0 or hour > 24)
    {
        cout << "Неверный формат, часы = " << hour << ", присваиваю значение 0" << endl;
        date->hour = 0;
    }
    else
        date->hour = hour;

    if (minute < 0 or minute > 60)
    {
        cout << "Неверный формат, минуты = " << minute << ", присваиваю значение 0" << endl;
        date->minute = 0;
    }
    else
        date->minute = minute;

    if (second < 0 or second > 60)
    {
        cout << "Неверный формат, секунды = " << second << ", присваиваю значение 0" << endl;
        date->second = 0;
    }
    else
        date->second = second;

    if (day < 1 or day > 31)
    {
        cout << "Неверный формат дней, автоматически выставляется значение 1" << endl;
        date->day = 1;
    }
    else
        date->day = day;

    if (month < 0 or month > 12)
    {
        cout << "Неверный формат месяцев, автоматически выставляется значение 1" << endl;
        date->month = 1;
    }
    else
        date->month = month;

    if (year < 0)
    {
        cout << "Неверный формат года, автоматически выставляется значение 2000" << endl;
        date->year = 2000;
    }
    else
        date->year = year;
}

void datetime::print_date(datetime date)
{
    cout << "-.-.-..-..-.-.-.-.-.-." << endl;
    cout << "Текущие время и дата:" << endl;
    cout << "Час: " << date.hour << endl;
    cout << "Минута: " << date.minute << endl;
    cout << "Секунда: " << date.second << endl;
    cout << "День: " << date.day << endl;
    cout << "Месяц: " << date.month << endl;
    cout << "Год: " << date.year << endl;
    cout << "----------------------" << endl << endl;
}

void datetime::print_info(datetime* date, int n)
{
    cout << "№ " << n << " Дата: " << date->day << "." << date->month << "." << date->year << ";\t";
    date->next_date(date);
    cout << "Следущая дата: " << date->day << "." << date->month << "." << date->year << ";\t";
    date->prev_date(date);
    date->prev_date(date);
    cout << "Предыдущая дата: " << date->day << "." << date->month << "." << date->year << endl;
    date->next_date(date);
}

void datetime::print_info_prev(datetime* date, int n)
{
    int n_mth = date->month;
    date->prev_date(date);
    int p_mth = date->month;
    date->next_date(date);
    if (n_mth == p_mth) {
        cout << "№ " << n << " Дата: " << date->day << "." << date->month << "." << date->year << ";\t";
        date->prev_date(date);
        cout << "Предыдущая дата дата: " << date->day << "." << date->month << "." << date->year << endl;
        date->next_date(date);
    }
    else {
        cout << "----> № " << n << " Не подходит по условию" << endl;
    }
}

bool check_day(int month, int day, int year)
{
    int month_31day[7] = { 1, 3, 5, 7, 8, 10, 12 }; 

    if (day < 1) 
        return false;

    if (month == 2)
    {
        if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
        {
            if (day > 29)
            {
                return false;
            }
        }
        else
        {
            if (day > 28)
            {
                return false;
            }
        }
    }

    for (int i = 0; i < 7; i++)
    {
        if (month == month_31day[i])
        {
            if (day > 31) 
                return false;
        }
        else
            if (day > 30)
                return false;
    }
    return true;
}

bool check_month(int month)
{
    if (month > 12 or month < 1) 
        return false;
    return true;
}

void datetime::next_date(datetime* date)
{
    if (check_day(date->month, date->day + 1, date->year))
        date->day++;
    else
    {
        date->day = 1;

        if (check_month(date->month + 1))
        {
            date->month++;
        }
        else
        {
            date->month = 1;
            date->year++
                ;
        }
    }
}

void datetime::prev_date(datetime* date)
{
    int month_31day[7] = { 1, 3, 5, 7, 8, 10, 12 };


    if (check_day(date->month, date->day - 1, date->year))
    {
        date->day--;
    }
    else
    {
        if (date->month - 1 == 2)
        {
            if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) 
            {
                date->day = 29;
            }
            else date->day = 28;
        }
        else

            for (int i = 0; i < 7; i++)
            {
                if (date->month == 1)
                    date->day = 31;
                else if (date->month - 1 == month_31day[i])
                    date->day = 31;
                else
                    date->day = 30;
            }
        if (check_month(date->month - 1))
            date->month--;
        else
        {
            date->month = 12;
            date->year--;
        }
    }
}


int get_count_lines(string file)
{
    ifstream f(file);
    string s;
    int count = 0;
    while (getline(f, s))
    {
        count++;
    }

    return count;
}

string** read_file(string file, int count_lines) 
{
    ifstream f(file);
    string line;

    string** lines = new string * [count_lines];

    for (int i = 0; i < count_lines; i++)
    {
        lines[i] = new string[6];
    }

    int j = 0;
    while (getline(f, line))
    {
        string buf;
        stringstream x;
        x << line;

        for (int i = 0; i < 6; i++)
        {
            x >> buf;
            lines[j][i] = buf;
        }
        j++;
    }


    f.close();
    return lines;
}