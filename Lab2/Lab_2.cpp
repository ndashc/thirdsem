﻿#include <iostream>
#include <fstream>
#include <time.h>
#include <string>
#include <sstream>
using namespace std;

class datetime
{
private:
    bool check_day(int, int, int);
    bool check_month(int);

public:
    int hour, minute, second, day, month, year;
    datetime(int hour, int minute, int second, int day, int month, int year) :  hour(hour), minute(minute), second(second), day(day), month(month), year(year)
    {
        if (hour < 0 or hour > 12) this->hour = 0;
        if (minute < 0 or minute > 60) this->minute = 0;
        if (second < 0 or second > 60) this->second = 0;
        if (day < 1 or day > 31) this->day = 1;
        if (month < 1 or month > 12) this->month = 1;      
        if (year < 1) this->year = 1;
        
    }
    void next_date();
    void prev_date();
    void print_date();
};

class get_date
{
private:
    int get_count_lines();
    string** read_file(int);
    string file = "data.txt";

public:
    int count_lines;
    datetime** dates = new datetime * [count_lines];
    get_date()
    {
        count_lines = get_count_lines();
    }

    datetime** get_value()
    {
        string** lines;
        datetime** dates;
        dates = new datetime* [count_lines];
        lines = read_file(count_lines);

        for (int i = 0; i < count_lines; i++)
        {
            dates[i] = new datetime(stoi(lines[i][0]), stoi(lines[i][1]), stoi(lines[i][2]), stoi(lines[i][3]), stoi(lines[i][4]), stoi(lines[i][5]));
        }
        return dates;
    }
};

class menu
{
private:
    int n = 0;
    void print_all(datetime** dates, get_date data)
    {
        for (int i = 0; i < data.count_lines; i++)
        {
            cout << "№ " << i + 1 << "\t";
            cout << "Текущая дата: ";
            dates[i]->print_date();
            cout << "\tСлед. дата: ";
            dates[i]->next_date();
            dates[i]->print_date();
            cout << "\tПред. дата: ";
            dates[i]->prev_date();
            dates[i]->prev_date();
            dates[i]->print_date();
            cout << endl;
            dates[i]->next_date();
        }
    }

    void print_prev(datetime** dates, get_date data)
    {
        for (int i = 0; i < data.count_lines; i++)
        {
            dates[i]->prev_date();
            int month = dates[i]->month;
            dates[i]->next_date();
            if (dates[i]->month == month) 
            {
                cout << "№ " << i + 1 << "\t";
                cout << "Текущая дата: ";
                dates[i]->print_date();
                cout << "\tПред. дата: ";
                dates[i]->prev_date();
                dates[i]->print_date();
                cout << endl;
            }else cout << "----> № " << i + 1 << " Не подходит по условию" << endl;
            
        }
    }

public:
    void choose(datetime** dates, get_date data)
    {
        int key = 1;
        do
        {
            cout << "\nВыберите действие: " << endl
                << "0 - завершить работу" << endl
                << "1 - вывести информацию о всех датах" << endl
                << "2 - вывести информацию о датах, для которых предыдущие не попадают на другой месяц" << endl
                << "-> ";
            cin >> key;

            if (key == 0)
            {
                cout << endl << "До свидания! :)\n";
                break;
            }
            else if (key == 1)
            {
                cout << "\nИнформаия о всех датах:\n";
                print_all(dates, data);

            }
            else if (key == 2)
            {
                cout << "\nДаты, для которых предыдущие не попадают на другой месяц:\n";
                print_prev(dates, data);
            }
            else
            {
                cout << "\nДействие не задано, попробуйте снова...\n\n";
            }

        } while (key != 0);

    }
};

class App
{
private:
    datetime** dates;
    get_date data;

public:
    void start()
    {
        dates = data.get_value();
        menu menu;
        menu.choose(dates, data);
    }

    ~App()
    {
        for (int i = 0; i < data.count_lines; i++)  delete[] dates[i];

        delete[] dates;
    }
};

int main()
{
    setlocale(LC_ALL, "Russian");
    App app;
    app.start();
    return 0;
}

bool datetime::check_day(int day, int month, int year)
{
    int month_31day[7] = { 1, 3, 5, 7, 8, 10, 12 };
    if (day < 1) return false;

    if (month == 2)
    {
        if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
        {
            if (day > 29) return false;
        }
        else if (day > 28) return false;

    }

    for (int i = 0; i < 7; i++)
    {
        if (month == month_31day[i])
        {
            if (day > 31) return false;
        }
        else if (day > 30) return false;
    }
    return true;
}

bool datetime::check_month(int month)
{
    if (month > 12 or month < 1) return false;
    return true;
}

void datetime::next_date()
{
    if (check_day(day + 1, month, year)) day++;
    else
    {
        day = 1;

        if (check_month(month + 1)) month ++;
        else
        {
            month = 1;
            year++;
        }
    }
}

void datetime::prev_date()
{
    int month_31day[7] = { 1, 3, 5, 7, 8, 10, 12 };


    if (check_day(day - 1, month, year))
    {
        day--;
    }
    else
    {
        if (month - 1 == 2)
        {
            if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
            {
                day = 29;
            }
            else day = 28;
        }
        else

            for (int i = 0; i < 7; i++)
            {
                if (month == 1)
                    day = 31;
                else if (month - 1 == month_31day[i])
                    day = 31;
                else
                    day = 30;
            }
        if (check_month(month - 1))
            month--;
        else
        {
            month = 12;
            year--;
        }
    }
}

void datetime::print_date()
{
    cout << day << "." << month << "." << year << "   " << hour << ":" << minute << ":" << second << "    ";
}

int get_date::get_count_lines()
{
    ifstream f(file);
    string s;
    int count = 0;
    while (getline(f, s)) count++;
    return count;
}

string** get_date::read_file(int count_lines)
{
    ifstream f(file);
    string line;
    string** lines = new string * [count_lines];

    for (int i = 0; i < count_lines; i++) lines[i] = new string[6];

    int j = 0;
    while (getline(f, line))
    {
        string buf;
        stringstream x;
        x << line;

        for (int i = 0; i < 6; i++)
        {
            x >> buf;
            lines[j][i] = buf;
        }
        j++;
    }

    f.close();
    return lines;
}